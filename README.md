
###testing

mvn clean install

###running

java -jar target/word-calculator-0.0.1-SNAPSHOT.jar src/test/resources/sample.in

###with time and memory on linux

time java -Xmx1024m -jar target/word-calculator-0.0.1-SNAPSHOT.jar src/test/resources/sample.in
