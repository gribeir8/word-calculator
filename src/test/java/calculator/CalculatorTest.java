package calculator;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

import com.google.common.io.Files;

import calculator.exception.CommandNotFoundException;
import calculator.exception.CommandParseException;
import calculator.exception.DefNameCharOutOfRangeException;
import calculator.exception.MaxDefNameLengthException;
import calculator.exception.MaxLinesException;
import calculator.exception.NoVarsCalcException;
import calculator.exception.TooManyVarsCalcException;
import calculator.exception.ValueAlreadySetException;
import calculator.exception.ValueOutOfRangeException;

public class CalculatorTest {
	
	public final static String TEST_PATH = "src/test/resources/";
	public final static String TEST_OUTPUT = TEST_PATH + "sample.out";
	
	@Before
	public void before() {
		
		Calculator.MAX_LINES = 2000;
		Calculator.clearDefs();
	}

	@Test
	public void testMain() throws Exception {
		
		String[] input = {TEST_PATH + "sample.in", TEST_OUTPUT};
	    Calculator.main(input);
	    assertTrue(Files.equal(new File(TEST_OUTPUT), new File(TEST_PATH + "sample.ans")));
	}
	
	@Test(expected = MaxLinesException.class)
	public void testFileSize() throws Exception {

		try {
			
			Calculator.MAX_LINES = 10;
			String[] input = {TEST_PATH + "sample.in", TEST_OUTPUT};

			Calculator.main(input);
		}catch(Exception e){
			System.out.println(e);
			throw e;
		}
	}

	@Test(expected = CommandNotFoundException.class)
	public void testCommandNotFound() throws Throwable {

		try {
			
		    Exception exception = assertThrows(RuntimeException.class, () -> {

				String[] input = {TEST_PATH + "sample.in.commandnotfound", TEST_OUTPUT};

				Calculator.main(input);
		    });

		    throw exception.getCause();
		}catch(Exception e){
			System.out.println(e);
			throw e;
		}
	}

	@Test(expected = CommandParseException.class)
	public void testAdditionalSpace() throws Throwable {

		try {
			
		    Exception exception = assertThrows(RuntimeException.class, () -> {

				String[] input = {TEST_PATH + "sample.in.extraspace", TEST_OUTPUT};

				Calculator.main(input);
		    });

		    throw exception.getCause();
		}catch(Exception e){
			System.out.println(e);
			throw e;
		}
	}

	@Test(expected = NumberFormatException.class)
	public void testValueNotANumber() throws Throwable {

		try {
			
			String[] input = {TEST_PATH + "sample.in.notinteger", TEST_OUTPUT};

			Calculator.main(input);
		}catch(Exception e){
			System.out.println(e);
			throw e;
		}
	}
	
	@Test(expected = ValueOutOfRangeException.class)
	public void testValueOutOfRange() throws Throwable {

		try {
			
		    Exception exception = assertThrows(RuntimeException.class, () -> {

				String[] input = {TEST_PATH + "sample.in.valueoutofrange", TEST_OUTPUT};

				Calculator.main(input);
		    });

		    throw exception.getCause();
		}catch(Exception e){
			System.out.println(e);
			throw e;
		}
	}

	@Test(expected = ValueAlreadySetException.class)
	public void testValueAlreadySet() throws Throwable {

		try {
			
		    Exception exception = assertThrows(RuntimeException.class, () -> {

				String[] input = {TEST_PATH + "sample.in.valuealreadyset", TEST_OUTPUT};

				Calculator.main(input);
		    });

		    throw exception.getCause();
		}catch(Exception e){
			System.out.println(e);
			throw e;
		}
	}

	@Test
	public void testSameDef() throws Exception {

		String[] input = {TEST_PATH + "sample.in.samedef", TEST_OUTPUT};
	    Calculator.main(input);
	    
	    assertEquals(4, Calculator.defsMap.get("foo").intValue());
	}
	
	@Test(expected = MaxDefNameLengthException.class)
	public void testNameTooBig() throws Throwable {

		try {
			
		    Exception exception = assertThrows(RuntimeException.class, () -> {

				String[] input = {TEST_PATH + "sample.in.nametoobig", TEST_OUTPUT};

				Calculator.main(input);
		    });

		    throw exception.getCause();
		}catch(Exception e){
			System.out.println(e);
			throw e;
		}
	}
	
	@Test(expected = DefNameCharOutOfRangeException.class)
	public void testNameCharOutOfRange() throws Throwable {

		try {
			
		    Exception exception = assertThrows(RuntimeException.class, () -> {

				String[] input = {TEST_PATH + "sample.in.charoutofrange", TEST_OUTPUT};

				Calculator.main(input);
		    });

		    throw exception.getCause();
		}catch(Exception e){
			System.out.println(e);
			throw e;
		}
	}	

	@Test(expected = TooManyVarsCalcException.class)
	public void testTooManyVarsCalc() throws Throwable {

		try {
			
		    Exception exception = assertThrows(RuntimeException.class, () -> {

				String[] input = {TEST_PATH + "sample.in.toomanyvars", TEST_OUTPUT};

				Calculator.main(input);
		    });

		    throw exception.getCause();
		}catch(Exception e){
			System.out.println(e);
			throw e;
		}
	}	

	@Test(expected = NoVarsCalcException.class)
	public void testNoVarsCalc() throws Throwable {

		try {
			
		    Exception exception = assertThrows(RuntimeException.class, () -> {

				String[] input = {TEST_PATH + "sample.in.novars", TEST_OUTPUT};

				Calculator.main(input);
		    });

		    throw exception.getCause();
		}catch(Exception e){
			System.out.println(e);
			throw e;
		}
	}	

	@Test(expected = CommandParseException.class)
	public void testOperatorSequence() throws Throwable {

		try {
			
		    Exception exception = assertThrows(RuntimeException.class, () -> {

				String[] input = {TEST_PATH + "sample.in.operatorsequence", TEST_OUTPUT};

				Calculator.main(input);
		    });

		    throw exception.getCause();
		}catch(Exception e){
			System.out.println(e);
			throw e;
		}
	}	
	
	@Test(expected = CommandParseException.class)
	public void testCalcEndsEquals() throws Throwable {

		try {
			
		    Exception exception = assertThrows(RuntimeException.class, () -> {

				String[] input = {TEST_PATH + "sample.in.calcnoequals", TEST_OUTPUT};

				Calculator.main(input);
		    });

		    throw exception.getCause();
		}catch(Exception e){
			System.out.println(e);
			throw e;
		}
	}	

	@Test
	public void testTrailingOperator() throws Throwable {

		String[] input = {TEST_PATH + "sample.in.trailingoperator", TEST_OUTPUT};
	    Calculator.main(input);
	    assertTrue(Files.equal(new File(TEST_OUTPUT), new File(TEST_PATH+"sample.ans.trailingoperator")));
	}	

}
