import java.io.BufferedReader;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Calculator {

	public final static int MAX_LINES = 2000;

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		System.out.println("file: ");
		System.out.println(args[0]);
		
		//check file lines nr
		long lines = checkLineNumbers(args[0], MAX_LINES);		
		System.out.println("lines nr: " + lines);
		
		Stream<String> stream = Files.lines(Paths.get(args[0]));
		//System.out.println("size: " + stream.count());
		stream.forEach( (line)-> {
			
			System.out.println("line: " + line);
		});
		stream.close();
		
	}
	
	  public static long checkLineNumbers(String fileName, int maxLines) throws Exception {

	      long lines = 0;
	      try(BufferedReader reader = new BufferedReader(new FileReader(fileName))){
	    	  
	          while (reader.readLine() != null) {
	        	  lines++;
	        	  if(lines>maxLines) {
	        		  
	        		  throw new Exception("File exceeds max lines number " + maxLines);
	        	  }
	          }
	      };
	      
          return lines;
	  }


}
