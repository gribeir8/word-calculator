package calculator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;

import calculator.exception.CommandNotFoundException;
import calculator.exception.CommandParseException;
import calculator.exception.DefNameCharOutOfRangeException;
import calculator.exception.MaxDefNameLengthException;
import calculator.exception.MaxLinesException;
import calculator.exception.NoVarsCalcException;
import calculator.exception.TooManyVarsCalcException;
import calculator.exception.ValueAlreadySetException;
import calculator.exception.ValueOutOfRangeException;

public class Calculator {

	private static final int MIN_VARS = 1;
	public static int MAX_LINES = 2000;

	public static enum COMMANDS {

		DEF("def"), CALC("calc"), CLEAR("clear");

		private final String command;

		COMMANDS(String command) {
			this.command = command;
		}

		@Override
		public String toString() {

			return this.command;
		}
	}

	public static final String SEPARATOR = "\\s+";
	public static final int[] VALUE_RANGE = { -1000, 1000 };
	// map of definitions - uses apache bidimap with getkey by value
	public static BidiMap<String, Integer> defsMap = new DualHashBidiMap<String, Integer>();
	public static final int MAX_DEF_LEN = 30;
	public static final String ALPHABET = "^[a-z]+$";
	public static final int MAX_CALC_VARS = 15;
	public static final String OPERATORS = "+-=";
	public static final String UNKNOWN = "unknown";
	public static final String DEFAULT_OUTPUT = "sample.out";

	public static void clearDefs() {

		Calculator.defsMap.clear();
	}

	public static void main(String[] args) throws Exception {

		if (args.length == 0) {
			System.out.println("No input. Exiting...");
			System.exit(1);
		}
		
		String firstLine = String.join(" ", args);
        boolean fileExists = checkInputFile(args[0]);
        
		// get out path
		String output = fileExists && args.length > 1 ? args[1] : DEFAULT_OUTPUT;
		try (FileWriter writer = new FileWriter(output)) {

			//if file exists ignore scanner and get in/out files
	        if(fileExists) {//get from input file

	    		// check file lines nr
	    		checkTotalLinesNumber(args[0], MAX_LINES);
				try (Stream<String> stream = Files.lines(Paths.get(args[0]))) {

					stream.forEach((line) -> {

						processLine(writer, line);

					});
				}
	        }else {//get from scanner

	            try(Scanner scan = new Scanner(System.in)){

		        	boolean firstDo = true;
	            	int lineNr = 0;
	            	do {

	            		++lineNr;
	            		checkMaxLines(lineNr);
	            		String line = firstLine;
	            		if(firstDo) {
	            			
	            			firstDo = false;
	            		}else {
	            			
	                		line = scan.nextLine();
	            		}
	            		processLine(writer, line);
	            		
	            	}while(scan.hasNextLine());
	            }
	        }			
		}
	}

	private static boolean checkInputFile(String fileName) {
		
		File f = new File(fileName);
        boolean fileExists = f.exists() && !f.isDirectory();
		return fileExists;
	}

	private static void processLine(FileWriter writer, String line) {
		
		line = line.trim();

		try {
			checkSeparator(line);
			checkUnknown(line);
		} catch (CommandParseException e) {
			throw new RuntimeException(e);
		}

		if (line.startsWith(COMMANDS.DEF.toString())) {// def

			processDefinition(line);

		} else if (line.startsWith(COMMANDS.CALC.toString())) {// calc

			processCalculation(line, writer);

		} else if (line.startsWith(COMMANDS.CLEAR.toString())) {// clear

			clearDefs();

		} else {

			Exception root = new CommandNotFoundException("Command not found at line: " + line
					+ "\n allowed: " + COMMANDS.DEF + "," + COMMANDS.CALC + "," + COMMANDS.CLEAR);
			throw new RuntimeException(root);
		}
	}

	private static void processCalculation(String line, FileWriter writer) {

		try {
			checkMaxMinVars(line);
			// check operators
			checkEndsWithEquals(line);
			checkOperatorSequence(line);
		} catch (TooManyVarsCalcException e) {
			throw new RuntimeException(e);
		} catch (NoVarsCalcException e) {
			throw new RuntimeException(e);
		} catch (CommandParseException e) {
			throw new RuntimeException(e);
		}
		String[] operators = OPERATORS.split("");
		String command = getRawCommand(line);
		command = command.trim();
		// checks unknown var prints unknown
		String vars = command.replaceAll("[" + OPERATORS + "]*", "");
		vars = vars.trim();
		List<String> names = Arrays.asList(vars.split(SEPARATOR));
		// check for unknown variables
		if (!defsMap.keySet().containsAll(names)) {
			// return and continue;
			String newLine = command + " " + operators[2] + " " + UNKNOWN + System.lineSeparator();
			printAndWriteToFile(writer, newLine);
		} else {

			// perform actual calculation
			List<String> calc = Arrays.asList(command.split(SEPARATOR));
			int result = 0;
			String nextOper = "";
			for (String item : calc) {
				if (defsMap.containsKey(item)) {
					if (nextOper.isEmpty()) {

						result = defsMap.get(item);
					} else if (nextOper.equals(operators[0])) {
						result += defsMap.get(item);
					} else if (nextOper.equals(operators[1])) {
						result -= defsMap.get(item);
					}
				} else if (item.equals(operators[0])) {// plus
					nextOper = operators[0];
				} else if (item.equals(operators[1])) {// minus
					nextOper = operators[1];
				} else {
					Exception root = new CommandParseException("Calc command incorrect: " + line);
				}
			}
			// display result
			if (defsMap.values().contains(result)) {

				String newLine = command + " " + operators[2] + " " + defsMap.getKey(result) + System.lineSeparator();
				printAndWriteToFile(writer, newLine);
			} else {

				String newLine = command + " " + operators[2] + " " + UNKNOWN + System.lineSeparator();
				printAndWriteToFile(writer, newLine);
			}
		}
	}

	private static void printAndWriteToFile(FileWriter writer, String newLine) {
		//print without line break
		System.out.print(newLine);
		try {
			writer.write(newLine);
			writer.flush();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private static void processDefinition(String line) {

		String[] parts = line.split(SEPARATOR);
		String name = parts[1];
		Integer value = Integer.parseInt(parts[2]);
		try {
			checkValueRange(value);
			checkSameValue(value);
			checkDefName(name);
		} catch (ValueOutOfRangeException e) {
			throw new RuntimeException(e);
		} catch (ValueAlreadySetException e) {
			throw new RuntimeException(e);
		} catch (MaxDefNameLengthException e) {
			throw new RuntimeException(e);
		} catch (DefNameCharOutOfRangeException e) {
			throw new RuntimeException(e);
		}
		defsMap.put(name, value);
	}

	private static void checkUnknown(String line) throws CommandParseException {
		
		if (line.contains(UNKNOWN)) {
			throw new CommandParseException("Reserved word " + UNKNOWN + " not allowed on input.");
		}
	}

	private static void checkEndsWithEquals(String line) throws CommandParseException {

		if (!line.endsWith(OPERATORS.split("")[2])) {
			throw new CommandParseException("Calc command does not ends with = operator: " + line);
		}
	}

	private static void checkMaxMinVars(String line) throws TooManyVarsCalcException, NoVarsCalcException {

		String command = getRawCommand(line);
		// checks unknown var prints unknown
		String vars = command.replaceAll("[" + OPERATORS + "]*", "");
		String[] names = vars.split(SEPARATOR);
		if (names.length > MAX_CALC_VARS) {

			throw new TooManyVarsCalcException("Too many vars on calc: " + vars + "\n allowed: " + MAX_CALC_VARS);
		} else if (names.length < MIN_VARS) {

			throw new NoVarsCalcException("No vars on calc. Minimum: " + MIN_VARS);
		}
	}

	private static String getRawCommand(String line) {
		
		String[] operators = OPERATORS.split("");
		String command = line.substring(COMMANDS.CALC.toString().length());// remove command
		if (command.endsWith(operators[2])) {
			command = command.substring(0, command.indexOf(operators[2]));// remove equals
		}
		return command;
	}

	protected static void checkDefName(String name) throws MaxDefNameLengthException, DefNameCharOutOfRangeException {

		if (name.length() > MAX_DEF_LEN) {

			throw new MaxDefNameLengthException("Def name too big: " + name);
		}
		if(!name.matches(ALPHABET)) {

			throw new DefNameCharOutOfRangeException(
					"Def name contains illegal char: " + name);
		}
	}

	protected static void checkSameValue(Integer value) throws ValueAlreadySetException {

		if (defsMap.values().contains(value)) {

			throw new ValueAlreadySetException("Value already Set: " + value);
		}
	}

	protected static void checkValueRange(Integer value) throws ValueOutOfRangeException {

		if (!(VALUE_RANGE[0] <= value && value <= VALUE_RANGE[1])) {
			throw new ValueOutOfRangeException(
					"Value is out of Range: " + VALUE_RANGE[0] + "<=>" + VALUE_RANGE[1] + " value: " + value);
		}
	}

	protected static void checkSeparator(String line) throws CommandParseException {

		String[] parts = line.split(" ");
		for (String part : parts) {

			if ("".equals(part)) {

				throw new CommandParseException("Extra spaces between command parts: " + line);
			}
		}
	}

	protected static void checkOperatorSequence(String line) throws CommandParseException {

		String[] operators = OPERATORS.split("");
		String anyOf = "[\\"+ operators[0]+"\\"+operators[1]+"\\"+operators[2]+"]";
		String expression = anyOf + SEPARATOR + anyOf;
		Pattern p = Pattern.compile(expression);
		Matcher matcher = p.matcher(line);
		if(matcher.find()) {

			throw new CommandParseException("Two or more operators with no var: " + line);
		}
	}

	protected static long checkTotalLinesNumber(String fileName, int maxLines) throws Exception {

		long lines = 0;
		try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {

			while (reader.readLine() != null) {
				lines++;
				if (lines > maxLines) {

					throw new MaxLinesException("File exceeds max lines number " + maxLines);
				}
			}
		}
		;

		return lines;
	}

	private static void checkMaxLines(int lineNr) throws Exception {

		if(lineNr>MAX_LINES) {

		    throw new Exception("Max lines number reached: " + MAX_LINES);
		}
	}
}
