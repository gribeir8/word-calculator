package calculator.exception;

public class ValueAlreadySetException extends Exception {

	public ValueAlreadySetException() {
		// TODO Auto-generated constructor stub
	}

	public ValueAlreadySetException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ValueAlreadySetException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ValueAlreadySetException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ValueAlreadySetException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
