package calculator.exception;

public class NoVarsCalcException extends Exception {

	public NoVarsCalcException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public NoVarsCalcException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public NoVarsCalcException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public NoVarsCalcException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public NoVarsCalcException() {
		// TODO Auto-generated constructor stub
	}

}
