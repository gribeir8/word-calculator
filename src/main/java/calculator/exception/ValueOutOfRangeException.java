package calculator.exception;

public class ValueOutOfRangeException extends Exception {

	public ValueOutOfRangeException() {
		// TODO Auto-generated constructor stub
	}

	public ValueOutOfRangeException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ValueOutOfRangeException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ValueOutOfRangeException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ValueOutOfRangeException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
