package calculator.exception;

public class CommandNotFoundException extends Exception {

	public CommandNotFoundException() {
		// TODO Auto-generated constructor stub
	}

	public CommandNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CommandNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public CommandNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CommandNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
