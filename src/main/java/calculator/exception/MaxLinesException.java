package calculator.exception;

public class MaxLinesException extends Exception {

	public MaxLinesException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public MaxLinesException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MaxLinesException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public MaxLinesException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public MaxLinesException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
