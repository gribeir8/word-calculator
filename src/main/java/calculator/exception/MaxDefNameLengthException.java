package calculator.exception;

public class MaxDefNameLengthException extends Exception {

	public MaxDefNameLengthException() {
		// TODO Auto-generated constructor stub
	}

	public MaxDefNameLengthException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public MaxDefNameLengthException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public MaxDefNameLengthException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public MaxDefNameLengthException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
