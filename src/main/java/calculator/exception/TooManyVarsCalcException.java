package calculator.exception;

public class TooManyVarsCalcException extends Exception {

	public TooManyVarsCalcException() {
		// TODO Auto-generated constructor stub
	}

	public TooManyVarsCalcException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public TooManyVarsCalcException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public TooManyVarsCalcException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public TooManyVarsCalcException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
